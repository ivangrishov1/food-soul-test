import { defineStore } from "pinia";
import {ref} from "vue";
import { throttle } from "lodash";

const url = "https://nominatim.openstreetmap.org/search?format=json&polygon=1&addressdetails=1&q="

export const useLocationStore = defineStore("locationStore", () => {
    const locations = ref<[]>([]);
    const searchLocation = ref<string>('');

    const getLocations = throttle(async (query: string) => {
        try {
            const res = await fetch(`${url}${query}`)
            locations.value = await res.json()

        } catch(error) {
            console.error(error)
        }
    }, 500)

    return {
        locations,
        searchLocation,
        getLocations,
    }
})